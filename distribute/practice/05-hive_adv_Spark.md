### Hive. Пользовательские функции

* Regular UDF: обрабатываем вход построчно,
* UDAF: аггрегация, n строк на вход, 1 на выходе,
* UDTF: 1 строка на вход, таблица (несколько строк и полей) на выходе,
* Wwindow functions: "окно" (несколько строк, *m*) на вход, несколько строк(*n*) на выходе (1 строка для каждого окна).

#### I. (Regular) User-defined functions

1. Для реализации UDF нужно создать Java-класс, являющийся наследником класса org.apache.hadoop.hive.ql.exec.UDF.
2. Реализовать в этом классе один или несколько методов evaluate(), в которых будет записана логика UDF.
3. Для сборки нужно подключить ещё один Jar-файл:
```
/opt/cloudera/parcels/CDH/lib/hive/lib/hive-exec.jar
```
4. Для использования UDF в запросе нужно:

  а) добавить собранный Jar-файл в Distributed cache (можно использовать относительный путь):
```
ADD JAR <path_to_jar>
```
При этом никаких дополнительных Jar-файлов в запросе можно не добавлять т.к. Jar с UDF уже содержит все необходимые коды.

  б) создать функцию на основе Java-класса UDF:
```
CREATE TEMPORARY FUNCTION <your_udf> AS 'com.your.OwnUDF';
```
> **Пример 10.** Реализовать UDF, которая возвращает тоже, что было подано ей на вход без каких-либо изменений.

На данном примере можно изучить синтаксис UDF и использовать его в дальнейших задачах. Код UDF и запроса с её использованием лежит в:
```
/home/velkerr/seminars/pd2018/12-hive_adv/1-example_udf[example.sql, Identity/]
```

> **Задача 11.** Реализовать UDF, принимающую на вход IP-адрес. На выход UDF выдаёт число - сумму октетов адреса. Можно использовать как таблицу Subnets, так и SerDeExample т.к. IP есть в обеих.

С UDF:
* много кода,
* только **Java** :(

Без UDF:
* Ещё больше кода (правда на SQL). Пример: `/home/velkerr/seminars/pd2018/12-hive_adv/2-sum_udf/query_without_udf.sql`
* Не всегда можно реализовать в 1 запрос => будут подзапросы => будет несколько Job (дольше).

#### II. User-defined table functions (UDTF)

От обычных UDF данный вид функций отличается тем, что на выходе может быть больше одной записи. Причём столбцов также может быть сгенерировано несколько, т.е. по одной записи на входе мы можем получить целую таблицу. Отсюда и название.

1. Для реализации UDTF нужно создать класс-наследника от org.apache.hadoop.hive.ql.udf.generic.GenericUDTF.
2. Логика UDTF пишется в 3 методах:

   а) `initialize()`: 
     - разбор входных данных (проверка количества аргументов и их типов), сохранение данных в ObjectInspector'ы
     - создание структуры выходной таблицы (названия и типы полей)
     
   б) `process()`: реализация механизма получения выходных данных из входных,
   
   в) `close()`: некий аналог cleanup() в MapReduce. Обрабатывает то, что не было обработано в `process()`.
  
3. Собираем Jar также, как и в случае с обычными UDF, однако для сборки подключить нужно не 1, а 2 дополнительных Jar:
```
/opt/cloudera/parcels/CDH/lib/hive/lib/hive-exec.jar
/opt/cloudera/parcels/CDH/lib/hive/lib/hive-serde.jar
```

> **Пример 12.** Реализовать UDTF, принимающую на вход IP-адрес. На выход выдаём этот же адрес, повторённый дважды.  Чтоб разграничить выводы для каждого IP, последней строкой в столбце выведите разделитель "-----".

|Вход|Выход|
|:----:|:---:|
|60.143.233.0|60.143.233.0|
||60.143.233.0|
||-----|
|14.226.82.0|14.226.82.0|
||14.226.82.0|
||-----|

Код UDTF и запроса с её использованием лежит в:
```
/home/velkerr/seminars/pd2018/12-hive_adv/3-example_udtf/[example.sql, CopyIp/]
```
> **Задача 13.** Реализовать UDTF, принимающую на вход IP-адрес. На выход выдаём список октетов адреса. Чтоб разграничить выводы для каждого IP, последней строкой в столбце выведите числовой разделитель (исп. Integer.MAX_VALUE).

|Вход|Выход|
|:----:|:---:|
|60.143.233.0|60|
||143|
||233|
||0|
||2147483647|
|14.226.82.11|14|
||226|
||82|
||11|
||2147483647|

#### III. User-defined aggregation functions (UDAF)

Позволяют реализовать свои функции наподобие `SUM()`, `COUNT()`, `AVG()`.

**Доп. литература.** [Programming hive](https://www.gocit.vn/files/Oreilly.Programming.Hive-www.gocit.vn.pdf), гл. 13 "Functions" (с. 163). 

# Apache Spark

* Скопируем ноутбук себе (проделываем на кластере) `cp -r /home/velkerr/seminars/pd2018/14-15-spark/06-spark-base_nb.ipynb ~/`, а также папку `/home/velkerr/seminars/pd2018/14-15-spark/images`.
* Логинисмся с пробросом портов (выполняем локально):  `ssh USER@mipt-client.atp-fivt.org -L 8088:mipt-master:8088 -L 18089:mipt-master:18089 -L PORT:localhost:PORT`.
* Запуск ноутбука (выполняем на кластере): `PYSPARK_DRIVER_PYTHON=jupyter PYSPARK_PYTHON=/usr/bin/python3 PYSPARK_DRIVER_PYTHON_OPTS='notebook --ip="*" --port=<PORT> --NotebookApp.token="<TOKEN>" --no-browser' pyspark2 --master=yarn --num-executors=2`
    * Выбираем себе порт начиная с 30000.  
* Открыть в браузере (локально): `localhost:PORT`.

_____________________________________________________________________________________

**Недостатки Hadoop MapReduce**
* Нужно писать очень много кода.
* Map-стадия пишет на локальный диск на нодах, reduce - в HDFS. Т.е. 2 записи на диск во время 1 Job'ы. Это сделано из соображений отказоустойчивости, но работает долго.
* Не работает с данными в реальном времени.

**Spark**
* Работает быстрее т.к. хранит промежуточные данные в памяти.
* Написана на Scala, но имеет также Java и Python API.
* Интерактивный. 
    * Есть Spark shell, который позволяет выполнять команды как в Jupyter-notebook. Spark-shell работает на Scala, Python.
    * Также можно сам Jupyter notebook настраивать под Spark.
* Также как и Hadoop, Spark может работать поверх YARN. А может и локально.

**Методы запуска Spark-задачи**
* Локальный
    * master:  `local[n]`, желательно n>1
* yarn-client
    * master: yarn-client
* yarn-cluster
    * master: yarn-cluster

Достоинства, недостатки?

> **Пример 14.** Посчитать частоту встречаемости слов в тексте "Горе от ума" Грибоедова (`/data/griboedov`). Отсортировать слова по кол-ву встречаемости.

Конфигурируем Spark context. [Параметры конфигурации](http://spark.apache.org/docs/latest/configuration.html#application-properties).
```
from pyspark import SparkContext, SparkConf

config = SparkConf().setAppName("griboedov").setMaster("local[3]") # в данном случае запуск локальный на 3 ядра CPU
spark_context = SparkContext(conf=config)
```
Преобразовываем RDD.
* [Список преобразований (transformations)](http://spark.apache.org/docs/latest/programming-guide.html#transformations)
    * RDD -> RDD
* [Список действий (actions)](http://spark.apache.org/docs/latest/programming-guide.html#actions)
    * RDD -> Object

Spark работает в ленивом режиме. При выполнении Transformations он строит граф вычислений. Реально же трансформации выполнятся при вызове Action.

Transformations бывают wide (содержат стадию Shuffle) и narrow (не содержат).

```
rdd = spark_context.textFile("/data/griboedov")
rdd2 = rdd.map(lambda x: x.strip().lower())
rdd3 = rdd2.flatMap(lambda x: x.split(" "))
rdd4 = rdd3.map(lambda x: (x,1))
rdd5 = rdd4.reduceByKey(lambda a, b: a + b).sortBy(lambda a: -a[1]) # суммируем "1" с одинаковыми ключами и сортируем по кол-ву встречаемости
words_count = rdd5.take(10)  # action! Берём TOP-10
```
Получаем результат и выводим.
```
for word, count in words_count:
    print word.encode("utf8"), count
```

* Запуск задачи: `spark2-submit <my_file.py> [-parameters]`

Логи пишутся в stderr поэтому чтоб увидеть только результат, перенаправьте stderr в /dev/null (`spark2-submit <my_file.py> 2>/dev/null`)
* Запуск Spark shell: `spark2-shell`.

Запустим сначала в локальном режиме, а затем в yarn-cluster.
Смотрим на Spark History server UI: http://mipt-master.atp-fivt.org:18089/

> **Задача 15.** При подсчёте отсеять пунктуацию и слова короче 3 символов. Считать только имена собственные. Именами собственными в данном случае будем считать такие слова, у которых 1-я буква заглавная, остальные - прописные.
