## Apache Kafka
Apache Kafka - это распределённый диспетчер, работающий на основе Zookeeper.

### Архитектура


### Терминология и особенности
* Единица представления в Kafka - логи
    * _чем отличаются логи от обычных тестовых строк, какая ключевая особенность их хранения?_
* Семантика доставки сообщений (из коробки) - At most once если репликация асинхронная и At least once если синхронная.
    * _Какие вы знаете семантики доставки сообщений? Для каких задач предпочительна каждая из них?_
* Асинхронная репликация
* Не хранит порядок сообщений (в отличие от обычных очередей)

Типы репликаций:
1. __Синхронная.__ Вновь пришедшие данные реплицируются синхронно на несколько нод.
    * Пример: Hadoop
2. __Асинхронная.__ Вновь пришедшие данные считаются записанными в систему тогда когда они записаны хотя бы на 1 ноду (она называется в этом случае **лидером**). Репликация на остальные ноды происходит уже после этого. Для каждой партиции лидером могут являться *различные ноды*.
    * Пример: Kafka

Асинхронная репликация работает быстрее, но при этом больше вероятность потери данных.

Отличия лидера в Kafka от мастера (NameNode) в Hadoop.
* Мастер контролирует выполнение задач рабочими узлами, но не делает за них вычисления.
* Лидер же имеет функционал рабочего узла, но при этом обладает специфич. свойствами (например, хранит самую свежую версию данных).
* В Kafka чтение сообщения происходит из мастера, остальные ноды нужны только на бекапа.

### Kafka CLI

Сервисы, которые использует Kafka:

| **Service** | **URL** |
|:-------:|:---:|
|Zookeeper servers|mipt-master.atp-fivt.org:2181|
||mipt-node01.atp-fivt.org:2181|
||mipt-node02.atp-fivt.org:2181|
|Kafka brokers|mipt-node04.atp-fivt.org:9092|
||mipt-node05.atp-fivt.org:9092|
||mipt-node06.atp-fivt.org:9092|
||mipt-node07.atp-fivt.org:9092|
||mipt-node08.atp-fivt.org:9092|
|Kafka bootstrap-servers| те же адреса, что и у брокеров|

#### Работа с топиками
Для того, чтоб работать с топиками нужно подключиться к Kafka. Для этого в параметры нужно передать хотя бы одну ноду Zookeeper. Команда обращается к Kafka, получает все сервера Zookeeper и с их помощью ищет нужный топик.

##### Выведем список топиков
```
kafka-topics --zookeeper mipt-node01.atp-fivt.org:2181 --list
```
##### Создадим топик
Для топика нужно определить:
* число партиций,
* фактор репликации,
* название топика.
```
kafka-topics --zookeeper mipt-node01.atp-fivt.org:2181 --create \
    --partitions 6 \
    --replication-factor 2 \
    --topic pd2018XX-topic
```

(вместо `XX` напишите 2 последние цифры своего аккаунта)
Снова делаем `list` и проверим, что топик создался. Максимальное кол-во партиций ограничено кол-вом kafka-брокеров.

##### Вывод информации о топике
Для этого вызываем `--describe`
```
kafka-topics --zookeeper mipt-node01.atp-fivt.org:2181 --describe --topic pd2018XX-topic
```
Что мы видим:
```
Topic:pd2018XX_topic	PartitionCount:6	ReplicationFactor:2	Configs:
	Topic: pd2018XX_topic	Partition: 0	Leader: 135	Replicas: 135,131	Isr: 135,131
	Topic: pd2018XX_topic	Partition: 1	Leader: 131	Replicas: 131,132	Isr: 131,132
	Topic: pd2018XX_topic	Partition: 2	Leader: 132	Replicas: 132,133	Isr: 132,133
	Topic: pd2018XX_topic	Partition: 3	Leader: 133	Replicas: 133,134	Isr: 133,134
	Topic: pd2018XX_topic	Partition: 4	Leader: 134	Replicas: 134,135	Isr: 134,135
	Topic: pd2018XX_topic	Partition: 5	Leader: 135	Replicas: 135,132	Isr: 135,132
```
* 6 партиций,
* 2 реплики у каждой,
* лидер и рабочие узлы для каждой партиции (например, видим, что 0-я и 5-я партиции имеют лидеров на 1 машине, а рабочие ноды у них отличаются)

Лидеры назначаются с помощью [Round-robin](https://ru.wikipedia.org/wiki/Round-robin_(%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC)). При этом, Kafka умеет балансировать нагрузку в зависимости от загруженности нод.

#### Чтение-запись в топик

* consumer читает данные, использует zookeeper
* producer записывает данные, использует kafka-broker

Kafka может читать данные из разных источников (HDFS, выходы приложений, базы данных, ханилище Amazon AWS). Можем также смешивать источники. Мы рассмотрим самый простой источник - консоль.

##### Прочитаем данные из топика данные в топик
```
kafka-console-consumer --zookeeper mipt-node01.atp-fivt.org:2181 --topic pd2018XX-topic
```
Команда сама не завершится т.к. она читает данные в реальном времени (ждёт, пока что-нибудь запишется в топик). Данных нет, прерываем команду.

##### Запишем данные в топик
Для этого нужно указать не Zookeeper-server, а список Kafka-брокеров. В списке достаточно 1 элемента.
```
kafka-console-producer --broker-list mipt-node05.atp-fivt.org:9092 --topic pd2018XX-topic
```
Запишем последовательно несколько чисел в топик и завершим команду.

Можно записывать всю последовательность одной командой: `seq 20 | kafka-console-producer --broker-list mipt-node05.atp-fivt.org:9092 --topic pd2018XX-topic`

##### Realtime чтение-запись.
Запускаем `tmux` и создаём 2 панели. В одной из них запускаем consumer, в другой - producer.

![IMAGE ALT TEXT HERE](assests/Kafka1.png)

Записываем числа и видим, что они синхронно отображаются в consumer'e.

Можно писать в один топик из нескольких источников и таким образом получится примитивный аналог TCP-чата.

Завершаем команды.

##### Kafka не хранит время поступления данных
Попробуем ещё раз прочитать данные... и видим, что снова ничего нет :(

По умолчанию consumer выводит только вновь поступившие сообщения, те, которые имеют самый свежий отступ (вспоминаем Offset'ы в Hadoop). Попробуем вывести весь топик.
```
kafka-console-consumer --zookeeper mipt-node01.atp-fivt.org:2181 --topic pd2018XX-topic --from-beginning
```
Данные выводятся, но в перемешанном виде (см. начало семинара, Kafka не хранит порядок сообщений в топике).

Мы можем вывести данные из конкретной партиции: 
```
kafka-console-consumer --zookeeper mipt-node01.atp-fivt.org:2181 --topic pd2018XX-topic --from-beginning --partition 0
```
И... не работает.

Дело в том, что для использования этой фичи нужно использовать new-style consumers. В первом приближении они отличаются от обычных консьюмеров тем, что у работают не брокерами, а с bootstrap-серверами. Это более общий интерфейс kafka, который знает и о брокерах, и о ZooKeeper'ах.
```
kafka-console-consumer --bootstrap-server mipt-node06.atp-fivt.org:9092 --topic pd2018XX-topic --new-consumer --from-beginning --partition 0
```
В рамках одной партиции видим упорядоченный результат.

##### User-defined скрипты для Kafka.
* [Java Doc по Kafka API](https://kafka.apache.org/10/javadoc/?org/apache/kafka/),
* [Python API](https://kafka-python.readthedocs.io/en/master/usage.html) тоже есть.

```
kafka-run-class kafka.tools.GetOffsetShell --broker-list  mipt-node05.atp-fivt.org:9092 --topic pd2018XX-topic --time -1
```
